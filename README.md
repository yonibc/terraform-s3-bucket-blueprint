Generic Bucket TerraForm Module
===============================

This module has the intent to provide a S3 bucket with standardized name using a prefix, enterprise name and suffix to guarantee uniqueness and allow enable some common features when using S3 bucket as a storage (this module does not support static website hosting neither logging yet).

# Use

To create a bucket with this module you need to insert the following peace of code on your own modules:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source = "git@bitbucket.org:credibilit/terraform-s3-bucket-blueprint.git?ref=<VERSION>"

  ... <parameters> ...
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

**Note**: Bucket policies must be enabled by the [s3_bucket_policy resource](https://www.terraform.io/docs/providers/aws/r/s3_bucket_policy.html).

## Input Parameters

The following parameters are used on this module:

- **enterprise**: The enterprise prefix for names and tags.
- **account**: The AWS account number ID.
- **environment**: The environment label for names and tags.
- **bucket_prefix**: A prefix for bucket name to ensure uniquiness.
- **acl**: The S3 bucket [Canned ACL](https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl).
- **force_destroy**: If the bucket must destroy all objects inside the bucket before delete the bucket (default: false). **USE WITH CARE!!!**
- **versioning_enabled**: If this bucket must versioning the objects. (default: false).
- **lifecycle_rule_id**: The default name for the embeded lifecycle created by this module. (default: default-lifecycle).
- **lifecycle_rule_prefix**: The key prefix inside S3 to apply the rule. (default: empty, apply to all bucket).
- **lifecycle_rule_enabled**: If the embbed rule is enabled or not. (default: false)
- **lifecycle_rule_days_sia**: How many days the objects will stay in default storage area before go to SIA. (default: 30 days).
- **lifecycle_rule_noncurrent_days_sia**: How many days the objects older versions will stay in default storage area before go to SIA. (default: 30 days).
- **lifecycle_rule_days_glacier**: How many days the objects will stay in SIA storeage area before go to Glacier. (default: 90 days).
- **lifecycle_rule_noncurrent_days_glacier**: How many days the objects older versions will stay in SIA storeage area before go to Glacier. (default: 90 days).
- **lifecycle_rule_expiration**: In how many days the objects will be deleted. (default: 365 days)
- **lifecycle_rule_noncurrent_expiration**: In how many days the objects older versions will be deleted. (default: 365 days).
- **policy**: A valid bucket policy JSON document. (optional)

## Output parameters

The following outputs can be accessed by the module which call this one.

- **id**: The bucket id.
- **arn**: The AWS ARN bucket reference.
- **hosted_zone_id**: The Route53 hosted zone id which the bucket is registered.
- **region**: The region which the bucket was created.
