resource "aws_s3_bucket" "bucket" {
  bucket = "${var.bucket_name}"
  acl = "${var.acl}"
  force_destroy = "${var.force_destroy}"

  versioning {
    enabled = "${var.versioning_enabled}"
  }

  policy = "${var.policy}"

  // A simple standard lyfecycle (disabled by default) to save costs
  lifecycle_rule {
    id = "${var.lifecycle_rule_id}"
    prefix = "${var.lifecycle_rule_prefix}"
    enabled = "${var.lifecycle_rule_enabled}"


    /* after ${var.lifecycle_rule_days_sia} days, send the objects to
     * Standard - Infrequent Access (SIA) storage area to save costs.
     *
     * Note: only objects lesser of 128KB in size are sent to SIA.
     *
     */
    transition {
      days = "${var.lifecycle_rule_days_sia}"
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days = "${var.lifecycle_rule_noncurrent_days_sia}"
      storage_class = "STANDARD_IA"
    }

    /* after ${var.lifecycle_rule_days_glacier} days, send the objects to
     * Glacier storage area to huge save costs.
     *
     * Note: objects are not immediatley accessible.
     *
     */
     transition {
       days = "${var.lifecycle_rule_days_glacier}"
       storage_class = "GLACIER"
     }

     noncurrent_version_transition {
       days = "${var.lifecycle_rule_noncurrent_days_glacier}"
       storage_class = "GLACIER"
     }

    /* after ${var.lifecycle_rule_expiration} days, delete the objects.
     *
     * Note: if versioning is not enabled, the object cannot be recovered in
     * any way.
     *
     */
    expiration {
      days = "${var.lifecycle_rule_expiration}"
    }

    noncurrent_version_expiration {
      days = "${var.lifecycle_rule_noncurrent_expiration}"
    }

  }

  tags {
    Name = "${var.bucket_name}"
  }

  count = "${var.enable ? 1 : 0}"
}
